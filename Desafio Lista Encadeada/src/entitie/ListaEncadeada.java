package entitie;

import service.Aluno;
import service.Celula;

public class ListaEncadeada {

	private Celula primeiro;
	private Celula ultimo;
	private Celula posicaoAtual;
	
	public void adiciona(Aluno aluno) {
		Celula celula = new Celula();
		celula.setValor(aluno);
		
		if (primeiro == null && ultimo == null) {
			primeiro = celula;
			ultimo = celula;
		}else {
			ultimo.setProximo(celula);
			ultimo = celula;			
		}
	}
	
	public boolean temProximo() {
		if (primeiro == null) {
			return false;
		}else if(posicaoAtual == null) {
			posicaoAtual = primeiro;
			return true;
		}else {
			boolean temProximo = posicaoAtual.getProximo() != null ? true : false;
			posicaoAtual = posicaoAtual.getProximo();
			return temProximo;
		}
		
	}

	public Celula getPosicaoAtual() {
		return posicaoAtual;
	}

	public void setPosicaoAtual(Celula posicaoAtual) {
		this.posicaoAtual = posicaoAtual;
	}	

}

