package main;

import java.util.Scanner;
import entitie.ListaEncadeada;
import service.Aluno;

public class Principal {
	
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		ListaEncadeada listaEncadeada = new ListaEncadeada();
		adicionaAluno(listaEncadeada);
		
		System.out.println("LISTA DE ALUNOS: ");
		System.out.println();
		while (listaEncadeada.temProximo()) {
			System.out.println(listaEncadeada.getPosicaoAtual().getValor().toString());
			System.out.println();
		}
	}

	public static void adicionaAluno(ListaEncadeada listaEncadeada) {
		String nome, matricula, email;
		
		char resposta = 's';
		while(resposta == 's') {
			System.out.println("DADOS DO ALUNO");
			System.out.println("Nome: ");
			nome = sc.nextLine();
			System.out.println("Matr�cula: ");
			matricula = sc.nextLine();
			System.out.println("Email: ");
			email = sc.nextLine();
			
			Aluno aluno = new Aluno(nome, matricula, email);
			listaEncadeada.adiciona(aluno);
			
			System.out.print("Deseja cadastrar um novo aluno? \n(s) sim    (n) n�o ");
			resposta = sc.nextLine().charAt(0);
		}
	}
}
